<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>

</head>

<body>

<div id="leftus">
			

      <?php if ($site_name) { ?><div id="site_name"><h3><a href="<?=$base_path?>"><? print $site_name; ?></a></h3></div><?php } ?>

	<?php if ($sidebar_left) { ?>
		<div id="sidebar_left">
			<?php print $sidebar_left ?>
		</div>
	<?php } ?>		

</div>


<div id="rightus">
<?php if ($sidebar_right) { ?>
	<div id="sidebar_right">
		<?php print $sidebar_right ?>
	</div>
<?php } ?>
</div>



<div id="primary_links" class="<?php print $layout ?>">

<?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' =>'links '.$layout, 'id' => 'navlist')) ?></div><?php } ?>
	
	
	
	<div id="content" class="<?php print $layout ?>">
		
		

		<?php if($breadcrumb) { ?>
			<div id="breadcrumb">
				<?php print $breadcrumb; ?>
			</div>
		<?php } ?>
        <h2 id="title"><?php print $title ?></h2>

        <div class="tabs"><?php print $tabs ?></div>
        <?php print $help ?>
        <?php print $messages ?>
        <?php print $content ?>
				<?php if (isset($secondary_links)) { ?><div id="secondary_links"><?php print theme('links', $secondary_links, array('class' =>'links', 'id' => 'navlist')) ?></div><?php } ?>

	<?php
		if (isset($footer_message) || isset ($feed_icons)) { ?>
		<div id="footer">
			
		<?php
			if(isset($footer_message)) echo "{$footer_message}";
			if(isset($feed_icons)) echo "{$feed_icons}";
		?></div><?php
		}
	?>

</div>


				


<?php print $closure ?>